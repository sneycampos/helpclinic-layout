<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Atendendo - Help Clinic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">

    <? include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h6>Detalhes do atendimento</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="font-weight-bold">Dados do Paciente</h6>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><a href="paciente.php" target="_blank">Ellyas de Oliveira Viana  <sup><i class="fa fa-external-link" aria-hidden="true"></i></sup></a></h5>
                                <p class="mb-0">47 anos</p>
                                <p class="mb-0">Rua 24 de Janeiro, 1910 - Teresina/PI</p>
<!--                                <p class="text-danger font-weight-bold">Detalhe Clínico: Diabético</p>-->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header pointer">
                                <h6 class="font-weight-bold" data-toggle="collapse" data-target="#historico-atendimentos" aria-expanded="true">Histórico de Atendimentos <sup><i class="fa fa-compress" aria-hidden="true"></i></sup></h6>
                            </div>
                            <div class="card-body collapse show" aria-expanded="true" id="historico-atendimentos">
                                <div class="list-group">
                                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h6 class="mb-1">01/03/2010</h6>
                                        </div>
                                        <p><b>Procedimento(s)</b>: Consulta Clínica</p>
                                        <p><b>Profissional</b>: Dr. Carlos Arantes da Silva</p>
<!--                                        <p><b>Hipótese Diagnóstica</b>: Lorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda autem eveniet excepturi hic illum in minima non numquam officiis quisquam quos rem rerum totam unde ut veritatis, vitae voluptates!</p>-->
                                    </a>

                                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h6 class="mb-1">05/14/2010</h6>
                                        </div>
                                        <p><b>Procedimento(s)</b>: Consulta Clínica</p>
                                        <p><b>Profissional</b>: Dr. Carlos Arantes da Silva</p>
<!--                                        <p><b>Hipótese Diagnóstica</b>: Lorem ipsum dolor sit amet, consectetur adipisicing elit. A assumenda autem eveniet excepturi hic illum in minima non numquam officiis quisquam quos rem rerum totam unde ut veritatis, vitae voluptates!</p>-->
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header"><h6 class="font-weight-bold">Em atendimento</h6></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="list-group" id="list-tab" role="tablist">
                                            <a class="list-group-item list-group-item-action active" id="list-anamnese" data-toggle="list" href="#anamnese" role="tab" aria-controls="anamnese">Anamnese</a>
                                            <a class="list-group-item list-group-item-action" id="list-diagnostico" data-toggle="list" href="#diagnostico" role="tab" aria-controls="diagnostico">Hipótese Diagnóstica</a>
                                            <a class="list-group-item list-group-item-action" id="list-exames" data-toggle="list" href="#exames" role="tab" aria-controls="exames">Exames e Procedimentos</a>
                                            <a class="list-group-item list-group-item-action" id="list-prescricao" data-toggle="list" href="#prescricao" role="tab" aria-controls="prescricao">Prescrição</a>
                                        </div>

                                        <div class="clearfix mt-3">
                                            <button class="btn btn-success" type="submit" value="Concluir Atendimento">Salvar</button>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="anamnese" role="tabpanel">

                                                <div class="form-group">
                                                    <label for="queixa">Queixa Principal</label>
                                                    <textarea name="queixa" id="queixa" rows="5" class="form-control"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label for=""></label>
                                                </div>
                                            </div>

                                            <div class="tab-pane fade" id="diagnostico" role="tabpanel">
                                                <div class="form-group">
                                                    <label for="diagnostico">Hipótese Diagnóstica</label>
                                                    <input type="text" name="diagnostico" id="diagnostico" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <label for="descricao_diagnostico">Descrição</label>
                                                    <textarea name="descricao_diagnostico" id="descricao_diagnostico" rows="5" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="exames" role="tabpanel" >
                                                <div class="form-group">
                                                    <label for="exames">Exames e Procedimentos</label>
                                                    <select name="exames[]" id="exames" class="form-control select2" multiple="multiple">
                                                        <option value="exame-1">Exame 1</option>
                                                        <option value="exame-2">Exame 2</option>
                                                        <option value="exame-3">Exame 3</option>
                                                        <option value="exame-4">Exame 4</option>
                                                        <option value="exame-5">Exame 5</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="prescricao" role="tabpanel" >
                                                <div class="form-group">
                                                    <label for="prescricao">Prescrição</label>
                                                    <textarea class="form-control" name="prescricao" id="prescricao" rows="8"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            placeholder: "Buscar Exame",
            tags: true,
            multiple: true,
            tokenSeparators: [','],
            allowClear: false,
            // ajax: {
            //     type:'post',
            //     url:'',
            //     dataType: "json",
            //     processResults: function (data) {
            //         return {
            //             results: data
            //         };
            //     },
            //     cache: true
            // },
            createTag: function(params) {
                return undefined;
            }
        });
    });
</script>
<script>
</script>
</body>
</html>
