<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./index.php">Help Clinic</a>
            <a class="navbar-brand hidden" href="./index.php">HC</a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="./index.php"> <i class="menu-icon fa fa-lg fa-home"></i>Página Inicial</a>
                </li>

                <h3 class="menu-title">Clínica</h3>
                <li class="active">
                    <a href="index.php">
                        <i class="menu-icon fa fa-calendar" aria-hidden="true"></i> Agendamentos
                    </a>
                </li>

                <li>
                    <a href="pacientes.php">
                        <i class="menu-icon fa fa-address-book" aria-hidden="true"></i> Pacientes
                    </a>
                </li>

                <li>
                    <a href="financeiro.php">
                        <i class="menu-icon fa fa-money" aria-hidden="true"></i> Financeiro
                    </a>
                </li>

                <h3 class="menu-title">Médicos</h3>

                <li>
                    <a href="atendimentos.php">
                        <i class="menu-icon fa fa-address-book" aria-hidden="true"></i> Atendimentos
                    </a>
                </li>



                <h3 class="menu-title">Configurações</h3>

                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-cogs"></i>
                        Cadastros
                    </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-user-md"></i><a href="profissionais.php">Profissionais</a></li>
                        <li><i class="menu-icon fa fa-h-square"></i><a href="consultorios.php">Consultórios</a></li>
                        <li><i class="menu-icon fa fa-newspaper-o"></i><a href="convenios.php">Convênios</a></li>
                        <li><i class="menu-icon fa fa-file-text-o"></i><a href="procedimentos.php">Procedimentos</a></li>
                        <li><i class="menu-icon fa fa-certificate"></i><a href="especialidades.php">Especialidades</a></li>
                    </ul>
                </li>

                <!--<li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-th"></i>
                        Pacientes
                    </a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="menu-icon fa fa-th"></i><a href="#">Lançar nova</a></li>
                    </ul>
                </li>-->

            </ul>
        </div>
    </nav>
</aside>