<?

$agendamentos = [];

$agendamentos = [
        [
            'consultorio'   => 'C1',
            'horario'       => '08:00',
            'paciente'      => 'Ellyas de Oliveira Viana',
            'profissional'  => 'Dr. Antonio Pereira da Silva',
            'especialidade' => 'Psiquiatra',
            'status'        => 'confirmado'
        ],
        [
            'consultorio'   => 'C1',
            'horario'       => '08:30',
            'paciente'      => 'Paulo Emilio de Melo Rosas Costa',
            'profissional'  => 'Dr. Antonio Pereira da Silva',
            'especialidade' => 'Psiquiatra',
            'status'        => 'cancelado'
        ],
        [
            'consultorio'   => 'C1',
            'horario'       => '09:00',
            'paciente'      => 'Marcos  Asael Silva',
            'profissional'  => 'Dr. Antonio Pereira da Silva',
            'especialidade' => 'Psiquiatra',
            'status'        => 'confirmado'
        ],
        [
            'consultorio'   => 'C1',
            'horario'       => '14:00',
            'paciente'      => 'Marcelo Vinicius Bezerra Calvet',
            'profissional'  => 'Dr. Antonio Pereira da Silva',
            'especialidade' => 'Psiquiatra',
            'status'        => 'confirmado'
        ]
    ];

?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Agendamentos - HelpClinic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">

    <? include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h4>Agendamentos</h4>
            </div>
            <div class="card-header">
                <a href="agendamento.php" class="btn btn-success btn-sm"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Agendar Atendimento</a>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <form action="" method="get" id="form-calendario">
                            <div class="form-row align-items-center">
                                <div class="form-group col-md-3">
                                    <label for="profissional">Profissional</label>
                                    <select name="profissional" id="profissional" class="form-control">
                                        <option value="1">Dr. Antonio Pereira da Silva </option>
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="calendario">Data</label>
                                    <input class="text-center form-control" type="text" name="date" id="calendario" value="<?=Date('Y-m-d')?>" />
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="">&nbsp;</label>
                                    <button type="submit" class="btn btn-primary form-control" value="Buscar">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <table class="table table-hover d-table">
                            <thead>
                            <tr>
                                <th class="w-10">Consultório</th>
                                <th class="w-10">Horário</th>
                                <th class="w-40">Paciente</th>
                                <th class="w-30">Profissional</th>
                                <th class="w-10 text-center">Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
//                            $range = range(strtotime("08:00"),strtotime("18:00"),30*60);

                                foreach($agendamentos AS $agendamento)
                                {
                                    $badge = ($agendamento['status'] == 'confirmado') ? 'success' : 'danger';
                                    $badgeIco = ($agendamento['status'] == 'confirmado') ? 'fa-calendar-check-o' : 'fa-calendar-times-o';

                                    ?>
                                    <tr>
                                        <td class="align-middle text-center">
                                            <span class="rounded-circle border border-primary p-2" style="height:60px;width:60px;"><?= $agendamento['consultorio'] ?></span>
                                        </td>
                                        <td class="align-middle">
                                            <?=$agendamento['horario']?>
                                        </td>
                                        <td class="align-middle">
                                            <span class="clearfix"><?=$agendamento['paciente']?></span>
                                            <span class="badge badge-<?= $badge ?>"><i class="fa <?= $badgeIco ?>" aria-hidden="true"></i> <?= $agendamento['status'] ?></span>
                                        </td>
                                        <td class="align-middle">
                                            <?=$agendamento['profissional']?>
                                        </td>
                                        <td class="align-middle text-center">
                                            <a href="#" class="btn btn-outline-secondary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</a>
                                        </td>
                                    </tr>
                                    <?
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.1.2/l10n/pt.js"></script>
<script src="assets/js/main.js"></script>
<script>
</script>
</body>
</html>
