<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Novo Paciente - HelpClinic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">

    <? include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h6 class="font-weight-bold">Novo Paciente</h6>
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="cpf" class=" form-control-label">CPF</label>
                        <input type="text" id="cpf" placeholder="Digite o CPF" class="form-control">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="nome" class=" form-control-label mt-2">Nome</label>
                        <input type="text" id="nome" placeholder="Digite o nome" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="endereco" class="form-control-label mt-2">Endereço</label>
                        <input type="text" id="endereco" placeholder="Digite o endereço" class="form-control">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="uf" class="form-control-label mt-2">Estado</label>
                        <select name="uf" id="uf" class="form-control">
                            <option value="">- escolha -</option>
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="endereco" class=" form-control-label mt-2">E-mail</label>
                        <input type="email" id="email" name="email" placeholder="email@email.com" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="telefone" class="form-control-label mt-2">Telefone</label>
                        <input type="text" id="telefone" placeholder="(99) 9999-9999" class="form-control">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="celular" class="form-control-label mt-2">Celular</label>
                        <input type="text" id="celular" placeholder="(99) 9 9999-9999" class="form-control">
                    </div>
                </div>

                <div class="form-row mt-3">
                    <div class="form-group col-md-3">
                        <button type="submit" class="btn btn-success" value="Salvar">Salvar</button>
                        <a href="pacientes.php" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>



            </div>
    </div>

    <div class="content mt-3">

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>

<script src="assets/js/main.js"></script>

</body>
</html>
