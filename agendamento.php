<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt-br"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt-br"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt-br"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Novo Agendamento - Help Clinic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">
    <? include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h6 class="font-weight-bold">Agendar Atendimento</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group col-md-2">
                        <label for="cpf">CPF do Cliente</label>
                        <input type="text" class="form-control" name="cpf" value="" placeholder="000.000.000-00" />

                        <a class="mt-2 d-block" href="javascript:;" data-toggle="modal" data-target="#modal-paciente">
                            <i class="fa fa-external-link" aria-hidden="true"></i> visualizar paciente
                        </a>
                    </div>
                    <div class="modal fade" id="modal-paciente" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Dados do Paciente</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="card-title"><a href="paciente.php" target="_blank">Ellyas de Oliveira Viana  <sup><i class="fa fa-external-link" aria-hidden="true"></i></sup></a></h5>
                                    <p class="mb-0">47 anos</p>
                                    <p class="mb-0">Rua 24 de Janeiro, 1910 - Teresina/PI</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="especialidade">Especialidade</label>
                        <select name="especialidade" id="especialidade" class="form-control">
                            <option value="">- escolha -</option>
                            <option value="1">Clínico Geral</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="profissional">Profissional</label>
                        <select name="profissional" id="profissional" class="form-control">
                            <option value="">- escolha -</option>
                            <option value="1">Carlos Ayres de Nobrega Fonseca</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-2">
                        <label for="data">Data e horário</label>
                        <input type="text" name="data" id="data" class="form-control" />
                    </div>
                </div>


                <div class="form-group mt-3">
                    <input type="submit" class="btn btn-success" value="Agendar">
                    <a href="index.php" class="btn btn-secondary">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.1.2/l10n/pt.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script>
    $("#data").flatpickr({
        enableTime: true,
        dateFormat: "d/m/Y H:i",
        locale: 'pt'
    });
</script>
</body>
</html>
