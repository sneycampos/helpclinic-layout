<?
$pacientes = [];

$pacientes = [
    [
        'nome' => 'ALEXANDRA FERREIRA DA SILVA',
        'data_nascimento' => '10/01/1985'
    ],
    [
        'nome' => 'ALINE FELIX DA SILVA SANTOS',
        'data_nascimento' => '10/01/1968'
    ],
    [
        'nome' => 'CRISTIANE BARBOSA PEREIRA GOMES',
        'data_nascimento' => '02/01/2001'
    ],
    [
        'nome' => 'DAIANA ALBINO BARBOSA',
        'data_nascimento' => '15/01/1990'
    ],
    [
        'nome' => 'DANIELLE ALVES DE SOUZA PORTO',
        'data_nascimento' => '21/01/1988'
    ],
    [
        'nome' => 'ELIVELTON DOS SANTOS PIRES',
        'data_nascimento' => '22/01/1945'
    ],
    [
        'nome' => 'FRANCISCO MARTINS RODRIGUES FILHO',
        'data_nascimento' => '02/01/1968'
    ],
    [
        'nome' => 'GRAZIELE DO NASCIMENTO GONCALVES DIAS',
        'data_nascimento' => '29/01/1990'
    ],
    [
        'nome' => 'OSILENE DA CONCEICAO DOS SANTOS',
        'data_nascimento' => '30/01/1991'
    ],
    [
        'nome' => 'JULIANA MARQUES DA SILVA',
        'data_nascimento' => '04/02/1978'
    ],
    [
        'nome' => 'KAREN CRISTINE DE OLIVEIRA PEDROSA',
        'data_nascimento' => '10/01/1985'
    ],

];

?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pacientes - Help Clinic</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://use.fontawesome.com/008e2f78ff.js"></script>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
<body>

<? include 'includes/side-menu.php'; ?>

<div id="right-panel" class="right-panel">

    <? include 'includes/header.php'; ?>

    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                <h4>Pacientes</h4>
            </div>
            <div class="card-header">
                <a href="paciente-registrar.php" class="btn btn-success btn-sm"><i class="fa fa-user-plus" aria-hidden="true"></i> Novo Paciente</a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <table class="table table-hover datatable">
                            <thead>
                            <tr>
                                <th class="col-1">#</th>
                                <th class="col-8">Nome</th>
                                <th class="col-2">Data Nascimento</th>
                                <th class="col-1">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?
                            $i=0;
                            foreach($pacientes AS $paciente)
                            {
                            $i++;
                            ?>
                                <tr>
                                    <td><?= str_pad($i, 4, 0, STR_PAD_LEFT) ?></td>
                                    <td><?=$paciente['nome']?></td>
                                    <td><?=$paciente['data_nascimento']?></td>
                                    <td>
                                        <div class="badge badge-success"><i class="fa fa-check" aria-hidden="true"></i>
                                            ativo
                                        </div>
                                    </td>
                                </tr>
                            <?
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"></script>
<script>
    $('.datatable').DataTable({
        // "pageLength" : 25,
        "autoWidth": false,
        "columns": [
            {"width": "5%"},
            {"width": "80%"},
            {"width": "10%"},
            {"width": "5%"}
        ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        "initComplete": function (settings, json) {
            // $('div.loading').hide();
            console.log('datatable loaded');
        }
    });
</script>
<script>
</script>
</body>
</html>
