$.noConflict();

jQuery(document).ready(function ($) {
    "use strict";

    $('#menuToggle').on('click', function (event) {
        $('body').toggleClass('open');
    });

    $('#calendario').flatpickr({
        altInput: true,
        altFormat: 'd/m/Y',
        locale: 'pt',
        onChange: function(selectedDates, dateStr, intance){
            // $('#form-calendario').submit();
        }
    });

});